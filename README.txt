CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 
INTRODUCTION
------------

The Entityreference Status module allow to use Entity Reference with widgets
that displays the current status of the entities to allow better distinction
between them.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/nicoloye/2505323

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2505323
   
REQUIREMENTS
------------
This module requires the following modules:

 * Entity Reference (https://www.drupal.org/project/entityreference)
 
INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
 
 * Go to any Entity Reference widget panel and set the widget to one of the
    available widgets :
    Autocomplete (with entity status)
    Autocomplete (Tags style, with entity status)
    Select list (with entity status)
    Check boxes / Radio buttons (with entity status)

CONFIGURATION
-------------
Any other field settings are inherited from Entity Reference, nothing special to do here.

MAINTAINERS
-----------

Current maintainers:
 * Nicolas Loye (nicoloye) - https://www.drupal.org/user/315225
 * Quentin Fahrner (Renrhaf) - https://www.drupal.org/user/2478454
 * Simon Göger (si.mon) - https://www.drupal.org/user/835096

This project has been sponsored by:
 * ARTE G.E.I.E.
 * Actency
 