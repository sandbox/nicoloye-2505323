<?php
/**
 * @file
 * Entity reference node status module, based on entityreference module.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function entityreference_status_ctools_plugin_directory($module, $plugin) {
  if ($module == 'entityreference' && $plugin == 'selection') {
    return 'plugins/entityreference/' . $plugin;
  }
}

/**
 * Implements hook_menu().
 */
function entityreference_status_menu() {
  $items = array();

  $items['entityreference-status/autocomplete/single/%/%/%'] = array(
    'title' => 'Entity Reference Status Autocomplete',
    'page callback' => 'entityreference_status_autocomplete_callback',
    'page arguments' => array(2, 3, 4, 5),
    'access callback' => 'entityreference_autocomplete_access_callback',
    'access arguments' => array(2, 3, 4, 5),
    'type' => MENU_CALLBACK,
  );
  $items['entityreference-status/autocomplete/tags/%/%/%'] = array(
    'title' => 'Entity Reference Status Autocomplete',
    'page callback' => 'entityreference_status_autocomplete_callback',
    'page arguments' => array(2, 3, 4, 5),
    'access callback' => 'entityreference_autocomplete_access_callback',
    'access arguments' => array(2, 3, 4, 5),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_field_widget_info().
 */
function entityreference_status_field_widget_info() {
  $widgets['entityreference_status_autocomplete'] = array(
    'label' => t('Autocomplete (with entity status)'),
    'description' => t('An autocomplete text field with entity status visible.'),
    'field types' => array('entityreference'),
    'settings' => array(
      'match_operator' => 'CONTAINS',
      'size' => 60,
      // We don't have a default here, because it's not the same between
      // the two widgets, and the Field API doesn't update default
      // settings when the widget changes.
      'path' => '',
    ),
  );

  $widgets['entityreference_status_autocomplete_tags'] = array(
    'label' => t('Autocomplete (Tags style, with entity status )'),
    'description' => t('An autocomplete text field with entity status visible.'),
    'field types' => array('entityreference'),
    'settings' => array(
      'match_operator' => 'CONTAINS',
      'size' => 60,
      // We don't have a default here, because it's not the same between
      // the two widgets, and the Field API doesn't update default
      // settings when the widget changes.
      'path' => '',
    ),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
    ),
  );

  $widgets['entityreference_status_options_select'] = array(
    'label' => t('Select list (with entity status)'),
    'description' => t('A select list field with entity status visible.'),
    'field types' => array('entityreference'),
    'settings' => array('allowed_values' => array(), 'allowed_values_function' => ''),
    'default_widget' => 'options_select',
    'default_formatter' => 'list_default',
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
    ),
  );

  $widgets['entityreference_status_options_buttons'] = array(
    'label' => t('Check boxes/radio buttons (with entity status)'),
    'description' => t('A boxes/radio buttons list field with entity status visible.'),
    'field types' => array('entityreference'),
    'settings' => array('allowed_values' => array(), 'allowed_values_function' => ''),
    'default_widget' => 'options_buttons',
    'default_formatter' => 'list_default',
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
    ),
  );

  return $widgets;
}

/**
 * Implements hook_field_widget_form().
 */
function entityreference_status_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Ensure that the entity target type exists before displaying the widget.
  $entity_info = entity_get_info($field['settings']['target_type']);
  if (empty($entity_info)) {
    return;
  }

  $entity_type = $instance['entity_type'];
  $entity = isset($element['#entity']) ? $element['#entity'] : NULL;
  $handler = entityreference_get_selection_handler($field, $instance, $entity_type, $entity);
  $field_id = drupal_html_id('field-name-' . $element['#field_name']);

  // Autocomplete fields.
  if ($instance['widget']['type'] == 'entityreference_status_autocomplete' || $instance['widget']['type'] == 'entityreference_status_autocomplete_tags'
    || $instance['widget']['type'] == 'entityreference_status_options_select' || $instance['widget']['type'] == 'entityreference_status_options_buttons') {
    // Attach main JS behaviors to the form.
    $js = drupal_get_path('module', 'entityreference_status') . '/js/entityreference_status.js';
    $form['#attached']['js'][] = $js;

    if ($instance['widget']['type'] == 'entityreference_status_autocomplete') {
      // We let the Field API handles multiple values for us, only take
      // care of the one matching our delta.
      if (isset($items[$delta])) {
        $items = array($items[$delta]);
      }
      else {
        $items = array();
      }
    }

    $entity_ids = array();
    $entity_labels = array();
    $entity_statuses = array();

    // Build an array of entities ID.
    foreach ($items as $item) {
      $entity_ids[] = $item['target_id'];
    }

    // Load those entities and loop through them to extract their labels.
    $entities = entity_load($field['settings']['target_type'], $entity_ids);
    foreach ($entities as $entity_id => $entity_item) {
      $label = $handler->getLabel($entity_item);
      $status = $handler->getStatus($entity_item);
      $key = "$label ($entity_id)";

      // If entity has a status.
      if (!is_null($status)) {
        $entity_statuses[] = $status;
      }

      // Labels containing commas or quotes must be wrapped in quotes.
      if (strpos($key, ',') !== FALSE || strpos($key, '"') !== FALSE) {
        $key = '"' . str_replace('"', '""', $key) . '"';
      }
      $entity_labels[] = $key;
    }

    // Prepare the autocomplete path.
    if (!empty($instance['widget']['settings']['path'])) {
      $autocomplete_path = $instance['widget']['settings']['path'];
    }
    else {
      $autocomplete_path = $instance['widget']['type'] == 'entityreference_status_autocomplete' ? 'entityreference-status/autocomplete/single' : 'entityreference-status/autocomplete/tags';
    }

    $autocomplete_path .= '/' . $field['field_name'] . '/' . $instance['entity_type'] . '/' . $instance['bundle'] . '/';
    // Use <NULL> as a placeholder in the URL when we don't have an entity.
    // Most webservers collapse two consecutive slashes.
    $id = 'NULL';
    if ($entity) {
      list($eid) = entity_extract_ids($entity_type, $entity);
      if ($eid) {
        $id = $eid;
      }
    }
    $autocomplete_path .= $id;

    if ($instance['widget']['type'] == 'entityreference_status_autocomplete') {
      $element += array(
        '#type' => 'textfield',
        '#maxlength' => 1024,
        '#default_value' => implode(', ', $entity_labels),
        '#autocomplete_path' => $autocomplete_path,
        '#size' => $instance['widget']['settings']['size'],
        '#element_validate' => array('_entityreference_autocomplete_validate'),
        '#attributes' => array(
          'class' => array('entityreference-status'),
        ),
      );

      // If a status can be displayed we add the class to the element.
      if (count($entity_statuses) > 0) {
        $element['#attributes']['class'][] = 'entityreference-status-' . $entity_statuses[0];
      }

      return array('target_id' => $element);
    }
    elseif ($instance['widget']['type'] == 'entityreference_status_autocomplete_tags') {
      $element += array(
        '#type' => 'textfield',
        '#maxlength' => 1024,
        '#default_value' => implode(', ', $entity_labels),
        '#autocomplete_path' => $autocomplete_path,
        '#size' => $instance['widget']['settings']['size'],
        '#element_validate' => array('_entityreference_autocomplete_tags_validate'),
        '#attributes' => array(
          'class' => array('entityreference-status'),
        ),
      );

      if ($instance['widget']['type'] == 'entityreference_status_autocomplete_tags') {
        // Load the required js for the tags field.
        $js = drupal_get_path('module', 'entityreference_status') . '/js/jquery.highlighttextarea.js';
        $css = drupal_get_path('module', 'entityreference_status') . '/css/jquery.highlighttextarea.css';
        if (empty($form['#attached']['js']) || !in_array($js, $form['#attached']['js'])) {
          $form['#attached']['js'][] = $js;
        }
        if (empty($form['#attached']['css']) || !in_array($css, $form['#attached']['css'])) {
          $form['#attached']['css'][] = $css;
        }

        // Initialize highlight textarea jquery plugin.
        $scripts = array();
        foreach ($entity_ids as $key => $value) {
          $scripts[$entity_statuses[$key]][$value] = str_replace(array('(', ')'), '.', $entity_labels[$key]);
        }
        $script_settings = array();
        foreach ($scripts as $key => $value) {
          $script_settings[] = "{words: ['(" . implode('|', $value) . ")'], css_class: 'entityreference-status-tags-" . $key . "'}";
        }
        drupal_add_js("jQuery('." . $field_id . " input.entityreference-status').highlightTextarea({words: [" . implode(',', $script_settings) . "]});", array('type' => 'inline', 'scope' => 'footer'));
      }

      return $element;
    }
    // The following code is taken from the options module.
    // @see: options_field_widget_form().
    elseif ($instance['widget']['type'] == 'entityreference_status_options_select'
      || $instance['widget']['type'] == 'entityreference_status_options_buttons') {
      // Abstract over the actual field columns, to allow different field types
      // to reuse those widgets.
      $value_key = key($field['columns']);

      $type = str_replace('entityreference_status_options_', '', $instance['widget']['type']);
      $multiple = $field['cardinality'] > 1 || $field['cardinality'] == FIELD_CARDINALITY_UNLIMITED;
      $required = $element['#required'];
      $has_value = isset($items[0][$value_key]);
      $properties = _options_properties($type, $multiple, $required, $has_value);

      // Prepare the list of options.
      $options = _options_get_options($field, $instance, $properties, $entity_type, $entity);
      if (!$required && !$multiple && $type == 'buttons') {
        $options['_none'] = t('N/A');
      }
      if (!$required && $type == 'select') {
        $options['_none'] = t('- None -');
      }

      // Put current field values in shape.
      $default_value = _options_storage_to_form($items, $options, $value_key, $properties);

      // Gather all options statuses.
      $options_statuses = array();
      foreach ($options as $id => $label) {
        if ($id != '_none') {
          $options_entity = entity_load($field['settings']['target_type'], array($id));
          if (isset($options_entity[$id]->status)) {
            $options_statuses[$id] = $options_entity[$id]->status;
          }
        }
      }

      switch ($type) {
        case 'select':
          $element += array(
            '#type' => 'select',
            '#default_value' => $default_value,
            // Do not display a 'multiple' select box if there is only
            // one option.
            '#multiple' => $multiple && count($options) > 1,
            '#options' => $options,
            '#attributes' => array(
              'class' => array('entityreference-status'),
            ),
          );

          // As form API doesn't allow us to add classes to specific options of
          // a select list without overriding the whole theme function,
          // we add them through JS.
          $selectors = array();
          foreach ($options as $id => $label) {
            if ($id != '_none') {
              $selectors[] = "jQuery('." . $field_id . " select.entityreference-status option[value=" . $id . "]').addClass('entityreference-status-" . $options_statuses[$id] . "');";
            }
          }
          drupal_add_js(implode(' ', $selectors), array('type' => 'inline', 'scope' => 'footer'));
          break;

        case 'buttons':
          // If required and there is one single option, preselect it.
          if ($required && count($options) == 1) {
            reset($options);
            $default_value = array(key($options));
          }

          // If this is a single-value field, take the first default value, or
          // default to NULL so that the form element is properly recognized as
          // not having a default value.
          if (!$multiple) {
            $default_value = $default_value ? reset($default_value) : NULL;
          }

          $element += array(
            '#type' => $multiple ? 'checkboxes' : 'radios',
            // Radio buttons need a scalar value.
            '#default_value' => $default_value,
            '#options' => $options,
            '#attributes' => array(
              'class' => array('entityreference-status'),
            ),
          );

          // Add classes to the boxes/radio buttons.
          foreach ($options as $id => $label) {
            if ($id != '_none') {
              $element += array(
                $id => array(
                  '#type' => $multiple ? 'checkbox' : 'radio',
                  '#return_value' => $id,
                  '#default_value' => $id,
                  '#prefix' => '<div class="entityreference-status-' . $options_statuses[$id] . '">',
                  '#suffix' => '</div>',
                ),
              );
            }
          }
          break;
      }

      $element += array(
        '#value_key' => $value_key,
        '#element_validate' => array('options_field_widget_validate'),
        '#properties' => $properties,
      );
      return $element;
    }
  }
}

/**
 * Menu callback: autocomplete the label of an entity.
 *
 * @param string $type
 *   The widget type (i.e. 'single' or 'tags').
 * @param string $field_name
 *   The name of the entity-reference field.
 * @param string $entity_type
 *   The entity type.
 * @param string $bundle_name
 *   The bundle name.
 * @param string $entity_id
 *   Optional; The entity ID the entity-reference field is attached to.
 *   Defaults to ''.
 * @param string $string
 *   The label of the entity to query by.
 *
 * @return string
 *   The JSON.
 */
function entityreference_status_autocomplete_callback($type, $field_name, $entity_type, $bundle_name, $entity_id = '', $string = '') {
  // If the request has a '/' in the search text, then the menu system will have
  // split it into multiple arguments and $string will only be a partial. We want
  //  to make sure we recover the intended $string.
  $args = func_get_args();
  // Shift off the $type, $field_name, $entity_type, $bundle_name, and $entity_id args.
  array_shift($args);
  array_shift($args);
  array_shift($args);
  array_shift($args);
  array_shift($args);
  $string = implode('/', $args);

  $field = field_info_field($field_name);
  $instance = field_info_instance($entity_type, $field_name, $bundle_name);

  return entityreference_status_autocomplete_callback_get_matches($type, $field, $instance, $entity_type, $entity_id, $string);
}

/**
 * Return JSON based on given field, instance and string.
 *
 * This function can be used by other modules that wish to pass a mocked
 * definition of the field on instance.
 *
 * @param string $type
 *   The widget type (i.e. 'single' or 'tags').
 * @param string $field
 *   The field array definition.
 * @param string $instance
 *   The instance array definition.
 * @param string $entity_type
 *   The entity type.
 * @param string $entity_id
 *   Optional; The entity ID the entity-reference field is attached to.
 *   Defaults to ''.
 * @param string $string
 *   The label of the entity to query by.
 *
 * @return string
 *   The JSON.
 */
function entityreference_status_autocomplete_callback_get_matches($type, $field, $instance, $entity_type, $entity_id = '', $string = '') {
  $matches = array();

  $entity = NULL;
  if ($entity_id !== 'NULL') {
    $entity = entity_load_single($entity_type, $entity_id);
    $has_view_access = (entity_access('view', $entity_type, $entity) !== FALSE);
    $has_update_access = (entity_access('update', $entity_type, $entity) !== FALSE);
    if (!$entity || !($has_view_access || $has_update_access)) {
      return MENU_ACCESS_DENIED;
    }
  }

  $handler = entityreference_get_selection_handler($field, $instance, $entity_type, $entity);

  if ($type == 'tags') {
    // The user enters a comma-separated list of tags. We only autocomplete
    // the last tag.
    $tags_typed = drupal_explode_tags($string);
    $tag_last = drupal_strtolower(array_pop($tags_typed));
    if (!empty($tag_last)) {
      $prefix = count($tags_typed) ? implode(', ', $tags_typed) . ', ' : '';
    }
  }
  else {
    // The user enters a single tag.
    $prefix = '';
    $tag_last = $string;
  }

  if (isset($tag_last)) {
    // Get an array of matching entities with extended data.
    $entity_labels = $handler->getReferencableEntitiesExtended($tag_last, $instance['widget']['settings']['match_operator'], 10);

    // Loop through the products and convert them into autocomplete output.
    foreach ($entity_labels as $values) {
      foreach ($values as $entity_id => $entity_data) {
        $label = $entity_data['label'];
        if (!is_null($entity_data['status'])) {
          $status = ' entityreference-status entityreference-status-' . $entity_data['status'];
        }

        $key = $label . ' (' . $entity_id . ')';
        // Strip things like starting/trailing white spaces,
        // line breaks and tags.
        $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(decode_entities(strip_tags($key)))));
        // Names containing commas or quotes must be wrapped in quotes.
        if (strpos($key, ',') !== FALSE || strpos($key, '"') !== FALSE) {
          $key = '"' . str_replace('"', '""', $key) . '"';
        }
        $matches[$prefix . $key] = '<span class="reference-autocomplete' . $status . '">' . $label . '</span>';
      }
    }
  }

  drupal_json_output($matches);
}


/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function entityreference_status_form_field_ui_widget_type_form_alter(&$form, &$form_state, $form_id) {
  $field = field_info_field($form['#field_name']);
  if ($field['settings']['handler'] !== 'status_selection_handler') {
    // Remove autocomplete status widgets.
    foreach (array_keys(entityreference_status_field_widget_info()) as $widget) {
      if (isset($form['basic']['widget_type']['#options'][$widget])) {
        unset($form['basic']['widget_type']['#options'][$widget]);
      }
    }

    // Warn the user.
    $form['basic']['widget_type']['#description'] .= '<br/>' . t('To use "autocomplete" and "autocomplete (tag style)" with entity status you should use the "Simple with status" handler.');
  }
}
