/**
 * @file
 * Entity Reference Status js operations for autocomplete fields.
 *
 * This prototype provides default behavior for entity selection on both
 * the default autocomplete widget and the tagging autocomplete widget.
 */

(function ($) {
    /**
     * Puts the currently highlighted suggestion into the autocomplete field.
     * Override of the default behavior to set an additional CSS class if needed.
     */
    Drupal.jsAC.prototype.select = function (node) {
        // Only customize behavior if the widget is with status.
        var statusWidget = $(this.input).hasClass('entityreference-status');
        if (statusWidget) {
            var widgetTagging = $(this.input).parents('.field-widget-entityreference-status-autocomplete-tags').length > 0;

            if (!widgetTagging) {
                // Setting variables for regular expression to be used later.
                var current_results, select_results;
                var re = /(entityreference-status-\S+)/g;

                // Get any existing entityreference status class and remove it.
                var current_class = $(this.input).attr('class');
                if ((current_results = re.exec(current_class)) !== null) {
                    console.log('debug', current_results);
                    $(this.input).removeClass(current_results[0]);
                }

                // Retrieve the entityreference status class from the selected entry
                // in the suggestion list and add it to the input field.
                var selected_class = $(this.selected).find('span').attr('class');
                if ((select_results = re.exec(selected_class)) !== null) {
                    $(this.input).addClass(select_results[0]);
                }
            }
            else {
                // Retrieve the entityreference status class from the selected entry
                // in the suggestion list and add it to the input field.
                var re = /(entityreference-status-\S+)/g;
                var selected_class = $(this.selected).find('span').attr('class');
                if ((select_results = re.exec(selected_class)) !== null) {
                  selected_class = select_results[0];
                }

                // We need to set the value a first time for the highlighter to be aware of the modification.
                this.input.value = $(node).data('autocompleteValue');

                // Retrieve the last word from the sequence (we can do that as entityreference
                // does not autocomplete any other word by default.
                var words = this.input.value.replace(/\(|\)/gi, '.');
                words = words.split(',');
                var word = words[words.length - 1];

                // Update highlighter settings.
                $(this.input).highlightTextarea('addWord', [selected_class, word]);
            }
        }

        this.input.value = $(node).data('autocompleteValue');
        $(this.input).trigger('autocompleteSelect', [node]);
    };
})(jQuery);
