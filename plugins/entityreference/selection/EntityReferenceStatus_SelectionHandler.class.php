<?php

/**
 * EntityReferenceStatus_SelectionHandler.
 *
 * Based on the generic handler, but with added entity status.
 * Forced to redefine every specific handlers, code is taken from
 * entity reference base handlers so take care when updating this module.
 */
class EntityReferenceStatus_SelectionHandler extends EntityReference_SelectionHandler_Generic implements EntityReferenceStatus_SelectionHandlerInterface {

  /**
   * Implements EntityReferenceHandler::getInstance().
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    $target_entity_type = $field['settings']['target_type'];

    // Check if the entity type does exist and has a base table.
    $entity_info = entity_get_info($target_entity_type);
    if (empty($entity_info['base table'])) {
      return EntityReference_SelectionHandler_Broken::getInstance($field, $instance);
    }

    // Try to use status custom handlers.
    if (class_exists($class_name = 'EntityReferenceStatus_SelectionHandler_Generic_' . $target_entity_type)) {
      return new $class_name($field, $instance, $entity_type, $entity);
    }
    else {
      // Type not supported, use default handlers.
      watchdog('entityreference_status', 'EntityReferenceStatus_SelectionHandler does not support type %type', array('%type' => $target_entity_type), WATCHDOG_WARNING);

      // Try to use an existing specific handler.
      if (class_exists($class_name = 'EntityReference_SelectionHandler_Generic_' . $target_entity_type)) {
        return new $class_name($field, $instance, $entity_type, $entity);
      }

      // Otherwise use our custom generic handler.
      return new EntityReferenceStatus_SelectionHandler($field, $instance, $entity_type, $entity);
    }
  }

  /**
   * Implements EntityReferenceHandler::getStatus().
   */
  public function getStatus($entity) {
    $target_type = $this->field['settings']['target_type'];
    return entity_access('view', $target_type, $entity) && isset($entity->status) ? $entity->status : NULL;
  }

  /**
   * Implements EntityReferenceHandler::getReferencableEntitiesExtended().
   */
  public function getReferencableEntitiesExtended($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $options = array();
    $entity_type = $this->field['settings']['target_type'];

    $query = $this->buildEntityFieldQuery($match, $match_operator);
    if ($limit > 0) {
      $query->range(0, $limit);
    }

    $results = $query->execute();

    if (!empty($results[$entity_type])) {
      $entities = entity_load($entity_type, array_keys($results[$entity_type]));
      foreach ($entities as $entity_id => $entity) {
        list(,, $bundle) = entity_extract_ids($entity_type, $entity);

        $options[$bundle][$entity_id]['label'] = check_plain($this->getLabel($entity));
        $options[$bundle][$entity_id]['status'] = $this->getStatus($entity);
      }
    }

    return $options;
  }
}

/**
 * PER TYPE OVERRIDES
 *
 * The major part of below code is taken from EntityReference_SelectionHandler_XXX.class.php files.
 * Some checking may be required when updating the entity reference module.
 *
 * @TODO
 *   see if we can make the use of PHP5 Traits for code sharing
 *   then we should extend the base EntityReference_SelectionHandler_Generic_XXXX class and use the Trait.
 */


/**
 * Override for the Node type.
 */
class EntityReferenceStatus_SelectionHandler_Generic_node extends EntityReferenceStatus_SelectionHandler {
  public function entityFieldQueryAlter(SelectQueryInterface $query) {
    // Adding the 'node_access' tag is sadly insufficient for nodes: core
    // requires us to also know about the concept of 'published' and
    // 'unpublished'. We need to do that as long as there are no access control
    // modules in use on the site. As long as one access control module is there,
    // it is supposed to handle this check.
    if (!user_access('bypass node access') && !count(module_implements('node_grants'))) {
      $base_table = $this->ensureBaseTable($query);
      $query->condition("$base_table.status", NODE_PUBLISHED);
    }
  }
}

/**
 * Override for the User type.
 */
class EntityReferenceStatus_SelectionHandler_Generic_user extends EntityReferenceStatus_SelectionHandler {
  public function buildEntityFieldQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityFieldQuery($match, $match_operator);

    // The user entity doesn't have a label column.
    if (isset($match)) {
      $query->propertyCondition('name', $match, $match_operator);
    }

    // Adding the 'user_access' tag is sadly insufficient for users: core
    // requires us to also know about the concept of 'blocked' and
    // 'active'.
    if (!user_access('administer users')) {
      $query->propertyCondition('status', 1);
    }
    return $query;
  }

  public function entityFieldQueryAlter(SelectQueryInterface $query) {
    if (user_access('administer users')) {
      // In addition, if the user is administrator, we need to make sure to
      // match the anonymous user, that doesn't actually have a name in the
      // database.
      $conditions = &$query->conditions();
      foreach ($conditions as $key => $condition) {
        if ($key !== '#conjunction' && is_string($condition['field']) && $condition['field'] === 'users.name') {
          // Remove the condition.
          unset($conditions[$key]);

          // Re-add the condition and a condition on uid = 0 so that we end up
          // with a query in the form:
          //    WHERE (name LIKE :name) OR (:anonymous_name LIKE :name AND uid = 0)
          $or = db_or();
          $or->condition($condition['field'], $condition['value'], $condition['operator']);
          // Sadly, the Database layer doesn't allow us to build a condition
          // in the form ':placeholder = :placeholder2', because the 'field'
          // part of a condition is always escaped.
          // As a (cheap) workaround, we separately build a condition with no
          // field, and concatenate the field and the condition separately.
          $value_part = db_and();
          $value_part->condition('anonymous_name', $condition['value'], $condition['operator']);
          $value_part->compile(Database::getConnection(), $query);
          $or->condition(db_and()
            ->where(str_replace('anonymous_name', ':anonymous_name', (string) $value_part), $value_part->arguments() + array(':anonymous_name' => format_username(user_load(0))))
            ->condition('users.uid', 0)
          );
          $query->condition($or);
        }
      }
    }
  }
}

/**
 * Override for the Comment type.
 */
class EntityReferenceStatus_SelectionHandler_Generic_comment extends EntityReferenceStatus_SelectionHandler {
  public function entityFieldQueryAlter(SelectQueryInterface $query) {
    // Adding the 'comment_access' tag is sadly insufficient for comments: core
    // requires us to also know about the concept of 'published' and
    // 'unpublished'.
    if (!user_access('administer comments')) {
      $base_table = $this->ensureBaseTable($query);
      $query->condition("$base_table.status", COMMENT_PUBLISHED);
    }

    // The Comment module doesn't implement any proper comment access,
    // and as a consequence doesn't make sure that comments cannot be viewed
    // when the user doesn't have access to the node.
    $tables = $query->getTables();
    $base_table = key($tables);
    $node_alias = $query->innerJoin('node', 'n', '%alias.nid = ' . $base_table . '.nid');
    // Pass the query to the node access control.
    $this->reAlterQuery($query, 'node_access', $node_alias);

    // Alas, the comment entity exposes a bundle, but doesn't have a bundle column
    // in the database. We have to alter the query ourself to go fetch the
    // bundle.
    $conditions = &$query->conditions();
    foreach ($conditions as $key => &$condition) {
      if ($key !== '#conjunction' && is_string($condition['field']) && $condition['field'] === 'node_type') {
        $condition['field'] = $node_alias . '.type';
        foreach ($condition['value'] as &$value) {
          if (substr($value, 0, 13) == 'comment_node_') {
            $value = substr($value, 13);
          }
        }
        break;
      }
    }

    // Passing the query to node_query_node_access_alter() is sadly
    // insufficient for nodes.
    // @see EntityReferenceHandler_node::entityFieldQueryAlter()
    if (!user_access('bypass node access') && !count(module_implements('node_grants'))) {
      $query->condition($node_alias . '.status', 1);
    }
  }
}

/**
 * Override for the File type.
 */
class EntityReferenceStatus_SelectionHandler_Generic_file extends EntityReferenceStatus_SelectionHandler {
  public function entityFieldQueryAlter(SelectQueryInterface $query) {
    // Core forces us to know about 'permanent' vs. 'temporary' files.
    $query->condition('status', FILE_STATUS_PERMANENT);

    // Access control to files is a very difficult business. For now, we are not
    // going to give it a shot.
    // @todo: fix this when core access control is less insane.
    return $query;
  }

  public function getLabel($entity) {
    // The file entity doesn't have a label. More over, the filename is
    // sometimes empty, so use the basename in that case.
    return $entity->filename !== '' ? $entity->filename : basename($entity->uri);
  }
}

/**
 * Override for the Taxonomy term type.
 */
class EntityReferenceStatus_SelectionHandler_Generic_taxonomy_term extends EntityReferenceStatus_SelectionHandler {
  public function entityFieldQueryAlter(SelectQueryInterface $query) {
    // The Taxonomy module doesn't implement any proper taxonomy term access,
    // and as a consequence doesn't make sure that taxonomy terms cannot be viewed
    // when the user doesn't have access to the vocabulary.
    $base_table = $this->ensureBaseTable($query);
    $vocabulary_alias = $query->innerJoin('taxonomy_vocabulary', 'n', '%alias.vid = ' . $base_table . '.vid');
    $query->addMetadata('base_table', $vocabulary_alias);
    // Pass the query to the taxonomy access control.
    $this->reAlterQuery($query, 'taxonomy_vocabulary_access', $vocabulary_alias);

    // Also, the taxonomy term entity exposes a bundle, but doesn't have a bundle
    // column in the database. We have to alter the query ourself to go fetch
    // the bundle.
    $conditions = &$query->conditions();
    foreach ($conditions as $key => &$condition) {
      if ($key !== '#conjunction' && is_string($condition['field']) && $condition['field'] === 'vocabulary_machine_name') {
        $condition['field'] = $vocabulary_alias . '.machine_name';
        break;
      }
    }
  }

  public function getReferencableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if ($match || $limit) {
      return parent::getReferencableEntities($match , $match_operator, $limit);
    }

    $options = array();

    // We imitate core by calling taxonomy_get_tree().
    $entity_info = entity_get_info('taxonomy_term');
    $bundles = !empty($this->field['settings']['handler_settings']['target_bundles']) ? $this->field['settings']['handler_settings']['target_bundles'] : array_keys($entity_info['bundles']);

    foreach ($bundles as $bundle) {
      if ($vocabulary = taxonomy_vocabulary_machine_name_load($bundle)) {
        if ($terms = taxonomy_get_tree($vocabulary->vid, 0, NULL, TRUE)) {
          foreach ($terms as $term) {
            $options[$vocabulary->machine_name][$term->tid] = str_repeat('-', $term->depth) . check_plain($term->name);
          }
        }
      }
    }

    return $options;
  }

  public function getReferencableEntitiesExtended($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if ($match || $limit) {
      return parent::getReferencableEntities($match , $match_operator, $limit);
    }

    $options = array();

    // We imitate core by calling taxonomy_get_tree().
    $entity_info = entity_get_info('taxonomy_term');
    $bundles = !empty($this->field['settings']['handler_settings']['target_bundles']) ? $this->field['settings']['handler_settings']['target_bundles'] : array_keys($entity_info['bundles']);

    foreach ($bundles as $bundle) {
      if ($vocabulary = taxonomy_vocabulary_machine_name_load($bundle)) {
        if ($terms = taxonomy_get_tree($vocabulary->vid, 0, NULL, TRUE)) {
          foreach ($terms as $term) {
            $options[$vocabulary->machine_name][$term->tid]['label'] = str_repeat('-', $term->depth) . check_plain($term->name);
            $options[$vocabulary->machine_name][$term->tid]['status'] = $this->getStatus($term);
          }
        }
      }
    }

    return $options;
  }
}