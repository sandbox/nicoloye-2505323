<?php

/**
 * @file
 * Define the plugin class and label.
 */

$plugin = array(
  'title' => t('Simple with status (with optional filter by bundle)'),
  'class' => 'EntityReferenceStatus_SelectionHandler',
);
