<?php

/**
 * @file
 * Override the abstract entity reference selection handler.
 */
interface EntityReferenceStatus_SelectionHandlerInterface extends EntityReference_SelectionHandler {
  /**
   * Return a list of referencable entities
   * with more data than just ID / Label.
   *
   * @return
   *   An array of referencable entities, which keys are entity ids and
   *   values are an array of :
   *   - labels (safe HTML) to be displayed to the user.
   *   - status boolean indicating the status of the entity.
   */
  public function getReferencableEntitiesExtended($match = NULL, $match_operator = 'CONTAINS', $limit = 0);

  /**
   * Return the status of a given entity.
   */
  public function getStatus($entity);
}